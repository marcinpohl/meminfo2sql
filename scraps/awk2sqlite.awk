#!/usr/bin/awk -f

BEGIN {
    FS = ","; # Assuming CSV format for illustration
    print "BEGIN TRANSACTION;" > "inserts.sql";
}

{
    # Construct the INSERT statement
    insert_cmd = "INSERT INTO table_name (column1, column2) VALUES ('" $1 "', '" $2 "');";
    print insert_cmd > "inserts.sql";
}

END {
    print "COMMIT;" > "inserts.sql";
    # Execute the SQLite command
    system("sqlite3 database_name < inserts.sql");
}
