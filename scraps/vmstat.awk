#!/usr/bin/awk -f 
#BEGIN { FS="[: ]+"; OFS=","; TZ="UTC";}
BEGIN { OFS=","; TZ="UTC";}
# prepending disk as the name of the first column makes the rest easier
#NR==2 { $0= "disk"$0;  split($0,colnames); print colnames[3],colnames[2],colnames[1] } #DEBUG
NR==2
{	$0= "disk"$0;  split($0,colnames); 
	for(key in colnames){
		if (key > 1 && key <6)
			{print "RD" colnames[key]}
	}
}

NR>2 {print strftime("%s"),$1,$2,$3,$4,$5,$6}

