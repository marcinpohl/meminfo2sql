#!/usr/bin/bash

set -euo pipefail

function memoryclearall() {
sudo swapoff -a && sudo swapon -a
sync; sync;
sudo /usr/sbin/sysctl -w vm.drop_caches=3
}


function pyvenvfat() {
rm -fr -- blah;

memoryclearall

python3 -mvenv --copies blah
blah/bin/pip install ipython pandas polars jupyter
sync; sync
}


function unpackkernelfromnet() {
memoryclearall
rm -fr -- linux-6.8.8/
wget -q https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-6.8.8.tar.xz -O- \
| xz -d --stdout \
| pv -s 1430050k \
| tar x
sync; sync
### This runs for about 12 secs on old 4thread laptop with SSD
}

function unpackkernelfrommem() {
cp -Rp linux-6.8.8.tar /dev/shm/
memoryclearall
pv -s 1430050k /dev/shm/linux-6.8.8.tar \
| tar x
sync; sync
### This runs for about 12 secs on old 4thread laptop with SSD
}

#unpackkernelfromnet
unpackkernelfrommem
