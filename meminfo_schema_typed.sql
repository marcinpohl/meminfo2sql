CREATE TABLE IF NOT EXISTS "memory_info"(
    "timestamp" INTEGER ,
    "metric" TEXT,
    "value" INTEGER,
    "unit" TEXT,
    PRIMARY KEY( "timestamp","metric" )
) WITHOUT ROWID;
