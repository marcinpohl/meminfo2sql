CREATE TABLE IF NOT EXISTS "memory_info"(
    "timestamp" TEXT,
    "metric" TEXT,
    "value" TEXT,
    "unit" TEXT
);
