﻿MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-variables
MAKEFLAGS += --no-builtin-rules
SHELL := /bin/bash

#MKFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
#ROOT_DIR    := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
#CURRENT_DIR := $(notdir $(patsubst %/,%,$(dir $(MKFILE_PATH))))
### GNU make also has 'CURDIR' variable built in

DISK_STATS_DB := disk_stats/disk_stats.db

disk_stats: $(DISK_STATS_DB)

$(DISK_STATS_DB):
	disk_stats/vmstat2sql.sh

disk_stats_view:
	echo "select * from disk_stats_changesWR where devname=='sda';" \
	| sqlite3 -tabs -header disk_stats/disk_stats.db

.PHONY: disk_stats $(DISK_STATS_DB) disk_stats_view
