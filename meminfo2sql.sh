#!/bin/bash

DB_FILE="memory_usage.db"
#SCHEMA="meminfo_schema.sql"
SCHEMA="meminfo_schema_typed.sql"

rm -f -- "$DB_FILE"
while true; do
    awk '
        BEGIN { FS="[: ]+"; OFS=","; TZ="UTC";}
        $1 ~ /^[A-Za-z]+:/
            {print strftime("%s"),$1,$2,$3}
        ' /proc/meminfo \
    | sqlite3 -init "$SCHEMA" "$DB_FILE" '.mode csv' '.import /dev/stdin memory_info'

    sleep 1
done


# select M1.timestamp, M1.value as 'Dirty Value', M2.metric as 'Writebacks', M2.value from memory_info M1 inner join memory_info M2 ON M1.timestamp == M2.timestamp  where M1.metric in ('Dirty') AND M2.metric in ('Writeback', 'WritebackTmp') ORDER BY M1.metric, M1.timestamp ;
# SELECT timestamp, metric,value from memory_info where metric in ("Dirty", "Writeback", "WritebackTmp") order by metric,timestamp asc ;'
#sqlite3 "$DB_FILE" 'SELECT * from memory_info  order by metric, timestamp;'
#{print strftime("%Y-%m-%dT%H:%M:%SZ", systime(), 1),$1,$2,$3}
