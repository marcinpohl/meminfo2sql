#!/usr/bin/awk -f
BEGIN {
    # Set the input field separator to whitespace
    FS="[ \t]+";
    OFS=",";
    # Print the header
    print "timestamp,interrupt,cpu0,cpu1,cpu2,cpu3,cpu4,cpu5,cpu6,cpu7"
}
/^\s+CPU/ { cores = NR; print cores; }
/^[0-9A-Z]+:/ {
    # Extract interrupt name and CPU values
    interrupt = $1;
    cpu_values = "";
    for (i = 2; i <= NF; i++) {
        cpu_values = cpu_values $i ",";
    }
    # Remove trailing comma
    sub(/,$/, "", cpu_values);
    # Print the timestamp, interrupt name, and CPU values as CSV
    print strftime("%s"), interrupt, cpu_values
} /proc/interrupts
