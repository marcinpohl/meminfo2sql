-- select * from sqlite_schema where name=='disk_stats';
CREATE TABLE IF NOT EXISTS "disk_stats"(
timestamp INTEGER,
devname TEXT,
RDreq INTEGER,
RDmgd INTEGER,
RDsec INTEGER,
RDtime INTEGER,
WRreq INTEGER,
WRmgd INTEGER,
WRsec INTEGER,
WRtime INTEGER,
IOcur_d INTEGER,
IOtime INTEGER,
IOwtime INTEGER,
DSRDreq INTEGER,
DSRDmgd INTEGER,
DSRDsec INTEGER,
DSRDtime INTEGER,
FLSHreq INTEGER,
FLSHtime INTEGER,
PRIMARY KEY( "timestamp","devname" )
) WITHOUT ROWID;

-- do not use this externally, exists only as helper
CREATE VIEW IF NOT EXISTS disk_stats_changesA AS
SELECT
    timestamp,
    devname,
    RDreq - LAG(RDreq, 1, 0) OVER (PARTITION BY devname ORDER BY timestamp) AS RDreq_d,
    RDmgd - LAG(RDmgd, 1, 0) OVER (PARTITION BY devname ORDER BY timestamp) AS RDmgd_d,
    RDsec - LAG(RDsec, 1, 0) OVER (PARTITION BY devname ORDER BY timestamp) AS RDsec_d,
    RDtime - LAG(RDtime, 1, 0) OVER (PARTITION BY devname ORDER BY timestamp) AS RDtime_d,
    WRreq - LAG(WRreq, 1, 0) OVER (PARTITION BY devname ORDER BY timestamp) AS WRreq_d,
    WRmgd - LAG(WRmgd, 1, 0) OVER (PARTITION BY devname ORDER BY timestamp) AS WRmgd_d,
    WRsec - LAG(WRsec, 1, 0) OVER (PARTITION BY devname ORDER BY timestamp) AS WRsec_d,
    WRtime - LAG(WRtime, 1, 0) OVER (PARTITION BY devname ORDER BY timestamp) AS WRtime_d,
    IOcur_d,
    IOtime - LAG(IOtime, 1, 0) OVER (PARTITION BY devname ORDER BY timestamp) AS IOtime_d,
    IOwtime - LAG(IOwtime, 1, 0) OVER (PARTITION BY devname ORDER BY timestamp) AS IOwtime_d,
    DSRDreq - LAG(DSRDreq, 1, 0) OVER (PARTITION BY devname ORDER BY timestamp) AS DSRDreq_d,
    DSRDmgd - LAG(DSRDmgd, 1, 0) OVER (PARTITION BY devname ORDER BY timestamp) AS DSRDmgd_d,
    DSRDsec - LAG(DSRDsec, 1, 0) OVER (PARTITION BY devname ORDER BY timestamp) AS DSRDsec_d,
    DSRDtime - LAG(DSRDtime, 1, 0) OVER (PARTITION BY devname ORDER BY timestamp) AS DSRDtime_d,
    FLSHreq - LAG(FLSHreq, 1, 0) OVER (PARTITION BY devname ORDER BY timestamp) AS FLSHreq_d,
    FLSHtime - LAG(FLSHtime, 1, 0) OVER (PARTITION BY devname ORDER BY timestamp) AS FLSHtime_d
FROM disk_stats;

CREATE VIEW IF NOT EXISTS disk_stats_changes AS
SELECT *
FROM (
    SELECT *
    FROM disk_stats_changesA
)
LIMIT -1 OFFSET 1;
-- this should skip the first line but does not

CREATE VIEW IF NOT EXISTS disk_stats_changesRD AS
SELECT
    timestamp,
    devname,
    RDreq_d,
    RDmgd_d,
    RDsec_d,
    RDtime_d,
    IOcur_d,
    IOtime_d,
    IOwtime_d
FROM disk_stats_changes;

CREATE VIEW IF NOT EXISTS disk_stats_changesWR AS
SELECT
    timestamp,
    devname,
    WRreq_d,
    WRmgd_d,
    WRsec_d,
    WRtime_d,
    IOcur_d,
    IOtime_d,
    IOwtime_d
    DSRDreq_d,
    DSRDmgd_d,
    DSRDsec_d,
    DSRDtime_d,
    FLSHreq_d,
    FLSHtime_d
FROM disk_stats_changes;
